# RETRO JUMP

This is a runner style retro game, inspired by 80's Atari 2600 games.

<div align="center">
  <img src="https://drive.google.com/uc?id=1rHldkX4fZ2mjy8N35OPGSSmIqSevaT-h&export=download">
</div>

You can download the Android app in the Google Play Store: https://play.google.com/store/apps/details?id=com.robles8bit.retrojump

## Features

* 40 stages, divided in four areas;
* 10 different kinds of enemies;
* Three special weapons: Double Jump, Slow Motion and Sudden Death;
* A store where you can buy the weapons using the coins you've collected on the game.

## Architecture

* The game is written in pure JavaScript, without using any frameworks or even jQuery;
* The animation runs on a canvas at 60 FPS;
* The resolution of 284x160 was chosen to emulate a "landscape" 80's video game.